<?php

namespace Janis\JanisConnector\Model;

use Janis\JanisConnector\Model\JanisCartService;
use Janis\JanisConnector\Api\ReloadPdpCalendarDataInterface;

class ReloadPdpCalendarData implements ReloadPdpCalendarDataInterface
{
    /**
     * @var JanisCartService
     */
    private $janisCartService;

    private $request;


    public function __construct(
        JanisCartService $janisCartService,
        \Magento\Framework\Webapi\Rest\Request $request
    )
    {
        $this->janisCartService = $janisCartService;
        $this->request = $request;
    }

    /**
     * @return array|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute()
    {
        $body = $this->request->getBodyParams();

        if( !isset($body['shippingType']) || !isset($body['dropoff']) ){
            return ['message' => 'Error: Falta un parametro'];
        }

        if( isset($body['skus']) )
            return $this->janisCartService->getSplitCarts($body['shippingType'], $body['dropoff'], $body['skus']);

        return $this->janisCartService->getSplitCarts($body['shippingType'], $body['dropoff']);
    }
}
